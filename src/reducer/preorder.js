
// const saleType = localStorage.getItem('sale_saleType')
// const seller = localStorage.getItem('sale_seller')
import moment from 'moment'
const init = {
    shippingDate: moment(),
    id:null,
    keyword:"",
}
export default (state=init, actions)=>{
    switch (actions.type) {
      case "PREORDER_SET_SHIPPINGDATE":
        return {
          ...state,
          shippingDate: actions.value,
        }
      case "PRODUCTS_SET_KEYWORD":
        return {
          ...state,
          keyword: actions.value,
        }
      default:
        return state
    }
  }