import { combineReducers } from 'redux';

import sale from './sale';
import product from './product';
import preorder from './preorder';
export default combineReducers({
	sale,
	product,
	preorder,
});
