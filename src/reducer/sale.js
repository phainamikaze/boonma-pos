
const saleType = localStorage.getItem('sale_saleType')
const seller = localStorage.getItem('sale_seller')
const init = {
    saleType: saleType,
    seller: seller,
    showDialogPay: false,
    sum:0,
}
export default (state=init, actions)=>{
    switch (actions.type) {
      case "SALE_SET_SALETYPE":
        return {
          ...state,
          saleType: actions.value,
        }
      case "SALE_SET_SELLER":
        return {
          ...state,
          seller: actions.value,
        }
      case "SALE_SHOW_DIALOGPAY":
        return {
          ...state,
          showDialogPay: actions.value,
        }
      case "SALE_SET_SUM":
        return {
          ...state,
          sum: actions.value,
        }
      default:
        return state
    }
  }