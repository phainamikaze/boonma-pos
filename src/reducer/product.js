
// const saleType = localStorage.getItem('sale_saleType')
// const seller = localStorage.getItem('sale_seller')
const init = {
    editDialog: false,
    id:null,
    keyword:"",
}
export default (state=init, actions)=>{
    switch (actions.type) {
      case "PRODUCTS_SET_EDITDIALOG":
        return {
          ...state,
          editDialog: actions.value,
          id: actions.id,
        }
      case "PRODUCTS_SET_KEYWORD":
        return {
          ...state,
          keyword: actions.value,
        }
      default:
        return state
    }
  }