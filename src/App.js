import React from 'react';
import Route from './Route';
import {SuspenseWithPerf} from 'reactfire';
import { Provider } from 'react-redux'
import store from './store'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
const App = ()=>{
  const theme = createMuiTheme({
        palette: {
          type: 'dark',
        },
      })

  return (
    <SuspenseWithPerf
      fallback={'Loading...'}
      traceId={'load-boonma-pos_web-app'}
    >
      <Provider store={store}>
      <ThemeProvider theme={theme}>
      <Route/>
      </ThemeProvider>
      </Provider>
    
    </SuspenseWithPerf>
  )
}

export default App;