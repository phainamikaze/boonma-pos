import React, { useState, useEffect } from 'react';
import {
    Typography,
    FormControlLabel,
    Grid,
    FormControl,
    Checkbox,
    FormHelperText,
    TextField,
    Snackbar,
    Button,
    TableRow,
    TableCell,
    Table,
    TableContainer,
    Paper,
    TableHead,
    TableBody,
} from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux'
import TablePreoderList from '../components/TablePreoderList'
import TablePreorderDelivery from '../components/TablePreorderDelivery'
import {
    useFirestoreCollectionData,
    useFirestore
  } from 'reactfire'
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const PreorderDelivery = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            รายการที่ต้องจัดส่ง
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
            </Grid>
            <Grid item xs={12}>
                <TablePreorderDelivery />
                <br/>
            </Grid>
        </Grid>
        </>
    )
}
export default PreorderDelivery