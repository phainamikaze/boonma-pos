import React from 'react';
import {
    Typography,
    Grid,
} from '@material-ui/core'
import PreorderForm from '../components/PreorderForm'


const Preorder = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            พรีออเดอร์
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <PreorderForm />   
            </Grid>
        </Grid>
        </>
    )
}
export default Preorder