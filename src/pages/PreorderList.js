import React, { useState, useEffect } from 'react';
import {
    Typography,
    FormControlLabel,
    Grid,
    FormControl,
    Checkbox,
    FormHelperText,
    TextField,
    Snackbar,
    Button,
    TableRow,
    TableCell,
    Table,
    TableContainer,
    Paper,
    TableHead,
    TableBody,
} from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux'
import TablePreoderList from '../components/TablePreoderList'
import TablePreorderSummary from '../components/TablePreorderSummary'
import {
    useFirestoreCollectionData,
    useFirestore
  } from 'reactfire'

import PreorderToolBar from '../components/PreorderToolBar'
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Preorder = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            รายการออเดอร์
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <TablePreoderList />
                <br/>
            </Grid>
        </Grid>
        </>
    )
}
export default Preorder