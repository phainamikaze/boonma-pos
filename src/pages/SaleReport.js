import React, { useEffect,useState } from 'react';
import {
    Typography,
    TextField,
    Grid,
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    IconButton,
} from '@material-ui/core'
import moment from 'moment'
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch } from 'react-redux'
import {useFirestoreCollectionData, useFirestore} from 'reactfire'
import TableSaleReport from '../components/TableSaleReport'
import SaleType from '../components/SaleType'
import PickDate from '../components/PickDate'

const FormInput = ()=>{
    const firestore = useFirestore();
    const baseRef = firestore.collection('pendingSale')
    const baseRef2 = firestore.collection('products');
    const [inputForm,setInputForm] = useState({amount:1})
    const handeleChange = (e)=>{
        setInputForm({
            ...inputForm,
            [e.target.name]: isNaN(e.target.value)?e.target.value:Number(e.target.value)
        })
    }
    const onAdd = () =>{
        baseRef2.add({
          name:inputForm.name,
          price:inputForm.price,
        }).then((result)=>{
          baseRef.doc(result.id).set(inputForm)
        })
    }
    return (
        <form >
            <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
                
                <Grid item xs={12} sm={6}>
                   
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="price" name="price" label="ราคา* (บาท)" variant="outlined" type="number" 
                    inputProps={{ min: "0", step: "1" }} fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="amount" name="amount" label="จำนวน" variant="outlined" type="number" defaultValue={1}
                    inputProps={{ min: "0", step: "1" }} fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                <Button size="large" variant="contained" color="primary" fullWidth onClick={onAdd}>
                    เพิ่ม
                </Button>
                </Grid>
            </Grid>
        </form>
    )
}
const SaleTable = ()=>{
    const firestore = useFirestore()
    const dispatch = useDispatch()
    const baseRef = firestore.collection('sale')
    const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
    const [sumOfSaleList,setSumOfSaleList] = useState(0)
    useEffect(()=>{
        const sum = rows.reduce((s,i)=>{
          console.log(i)
            return (i.price*i.amount)+s
        },0)
        setSumOfSaleList(sum)
        dispatch({
          type:'SALE_SET_SUM',
          value: sum,
        })
    },[rows])
    const handleDelete = (id)=>{
      baseRef.doc(id).delete()
    }
    const handleIncrement = (id,amount,f)=>{
      if((amount+f)>=1){
        baseRef.doc(id).update({
          amount: amount+f
        });
      }
    }
    return (
        <>
        <Grid container justify="space-between" spacing={1}>
            <Grid item>
                <Typography variant="h4" gutterBottom>
                ยอดขาย :
                </Typography>
            </Grid>
            <Grid item >
                <Typography variant="h4" gutterBottom>
                {sumOfSaleList}
                </Typography>
            </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table size="small" aria-label="saleList">
            <TableHead>
              <TableRow>
                <TableCell>วันเวลา</TableCell>
                <TableCell align="right">ขายผ่าน</TableCell>
                <TableCell align="right">ราคา</TableCell>
                <TableCell>รวม</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {moment(row.createAt.seconds*1000).calendar()}
                  </TableCell>
                  <TableCell align="right">
                    {row.saleType}
                    </TableCell>
                  <TableCell align="right">{row.price}</TableCell>
                  <TableCell align="right">
                    {row.sum}
                    <IconButton aria-label="delete" onClick={()=>{handleDelete(row.id)}} size="small">
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </>
      );
}

const Sale = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            ราบงานการขายประจำวันที่
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <FormInput />   
            </Grid>
            <Grid item xs={12}>
                <TableSaleReport />
                <br/>
            </Grid>
        </Grid>
        </>
    )
}
export default Sale