import React, { useState,useEffect } from 'react';
import MUIDataTable from "mui-datatables";
import {
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Grid,
  TextField,
  Dialog,
  Fab,
  Button,
  Slide,
  IconButton
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {useFirestoreCollectionData, useFirestore, SuspenseWithPerf} from 'reactfire'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  TextField: {
    marginTop: theme.spacing(2),
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(5),
    right: theme.spacing(5),
  },
}));
const AddDialog = ({open,onClose})=>{
  const classes = useStyles();
  const firestore = useFirestore()
  const [valueForm,setValueForm] = useState({name:'',order:1})
  const baseRef = firestore.collection('productCategory')
  const handleChange = (e)=>{
    setValueForm({...valueForm,[e.target.name]:e.target.value})
  }
  const handleAdd = ()=>{
    onClose()
    baseRef.add({
      name: valueForm.name,
      order: Number(valueForm.order),
    }).then(()=>{
      setValueForm({name:'',order:1})
    })
  }
  return (
    <Dialog
      maxWidth="sm"
      fullWidth={true}
      open={open}
      TransitionComponent={Transition}
      keepMounted={false}
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">เพิ่มหมวดหมู่สินค้า</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          <SuspenseWithPerf
            fallback="waiting for progress..."
            traceId="dialog-productCategory-add"
          >
            <TextField id="name" label="ชื่อหมวดหมู่" name="name"  value={valueForm.name} 
              onChange={handleChange} variant="outlined" type="text" fullWidth={true} className={classes.TextField} autoFocus={true}/>
            <TextField id="order" label="ลับดับ" name="order" value={valueForm.order} 
            onChange={handleChange} variant="outlined" type="number" fullWidth={true} className={classes.TextField} />
            </SuspenseWithPerf>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
      <Button size="large" onClick={onClose} variant="outlined" color="primary">
          ยกเลิก
        </Button>
        <Button size="large" fullWidth variant="contained" color="primary" onClick={handleAdd}>
          เพิ่ม
        </Button>
      </DialogActions>
    </Dialog>
  );
}
const AddButton = ()=>{
  const classes = useStyles();
  const [open,setOpen] = useState(false)
  return (<>
    <Fab aria-label="Add" className={classes.fab} color="primary" onClick={()=>setOpen(true)}>
    <AddIcon />
  </Fab>
  <AddDialog open={open} onClose={()=>setOpen(false)}/>
  </>)
}
const EditDialog = ({open,id,onClose,tableMeta})=>{
  const classes = useStyles();
  const firestore = useFirestore()
  const [valueForm,setValueForm] = useState({name:'',order:1})
  const baseRef = firestore.collection('productCategory')
  const handleChange = (e)=>{
    setValueForm({...valueForm,[e.target.name]:e.target.value})
  }
  useEffect(()=>{
    setValueForm({name:tableMeta.rowData[0],order:tableMeta.rowData[1]})
  },[tableMeta])
  const handleEdit = ()=>{
    onClose()
    baseRef.doc(id).set({
      name: valueForm.name,
      order: Number(valueForm.order),
    })
  }
  return (
    <Dialog
      maxWidth="sm"
      fullWidth={true}
      open={open}
      TransitionComponent={Transition}
      keepMounted={false}
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">แก้ไขหมวดหมู่สินค้า</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          <SuspenseWithPerf
            fallback="waiting for progress..."
            traceId="dialog-productCategory-add"
          >
            <TextField id="name" label="ชื่อหมวดหมู่" name="name"  value={valueForm.name} 
              onChange={handleChange} variant="outlined" type="text" fullWidth={true} className={classes.TextField} autoFocus={true}/>
            <TextField id="order" label="ลับดับ" name="order" value={valueForm.order} 
            onChange={handleChange} variant="outlined" type="number" fullWidth={true} className={classes.TextField} />
            </SuspenseWithPerf>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
      <Button size="large" onClick={onClose} variant="outlined" color="primary">
          ยกเลิก
        </Button>
        <Button size="large" fullWidth variant="contained" color="primary" onClick={handleEdit}>
          บันทึก
        </Button>
      </DialogActions>
    </Dialog>
  );
}
const ItemActions = ({id,tableMeta})=>{
  const firestore = useFirestore()
  const baseRef = firestore.collection('productCategory')
  const [open,setOpen] = useState(false)
  const handleEdit = ()=>{
    setOpen(true)
  }
  const handleDelete = ()=>{
    if(window.confirm(`ยืนยันการลบ ?`)){
      baseRef.doc(id).delete()
    }
  }
  return (<><IconButton onClick={handleEdit}>
    <EditIcon/>
  </IconButton>
  <IconButton onClick={handleDelete}>
    <DeleteIcon/>
  </IconButton><EditDialog tableMeta={tableMeta} open={open} id={id} onClose={()=>setOpen(false)} /> </>)
}
const ProductCategory =()=>{
  const columns = [
    {
      name: "name",
      label: "หมวดหมู่",
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: "order",
      label: "เรียงลำดับ",
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: "id",
      label: " ",
      options: {
        filter: true,
        sort: true,
        customBodyRender:(value, tableMeta, updateValue) => {
          return <ItemActions id={value} tableMeta={tableMeta}/>
        }
      }
    },
  ]
  const options = {
    download: false,
    print: false,
    filter: false,
    selectableRows:'none',
  }
  const firestore = useFirestore()
  const baseRef = firestore.collection('productCategory').orderBy('order')
  const data = useFirestoreCollectionData(baseRef, { idField: 'id' ,startWithValue:[] })

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MUIDataTable 
            title={"หมวดหมู่สินค้า"} 
            data={data} 
            columns={columns} 
            options={options} 
          />
        </Grid>
      </Grid>
      <AddButton />
    </>
    )
}
export default ProductCategory