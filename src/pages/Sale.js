import React, { useEffect,useState } from 'react';
import {
    Typography,
    TextField,
    Grid,
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    IconButton,
} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch } from 'react-redux'
import {useFirestoreCollectionData, useFirestore} from 'reactfire'
import QuickSale from '../components/QuickSale'
import SaleType from '../components/SaleType'
import DialogPay from '../components/DialogPay'
import debounce from '../debounce'
const FormInput = ()=>{
    const firestore = useFirestore();
    const baseRef = firestore.collection('pendingSale');
    const baseRef2 = firestore.collection('products');
    const [inputForm,setInputForm] = useState({amount:1})
    const handeleChange = (e)=>{
        setInputForm({
            ...inputForm,
            [e.target.name]: isNaN(e.target.value)?e.target.value:Number(e.target.value)
        })
    }
    const onAdd = () =>{
        baseRef2.add({
          name:inputForm.name,
          price:inputForm.price,
        }).then((result)=>{
          baseRef.doc(result.id).set(inputForm);
        })
    }
    const handleScan = debounce(kw => {
      console.log(kw)
    }, 1000)
    return (
        <form >
            <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
                <Grid item xs={12} sm={1}>
                    <TextField id="code" name="code" label="code" variant="outlined" fullWidth onChange={e=>handleScan(e.target.value)} autoFocus={true}/>
                </Grid>
                <Grid item xs={12} sm={5}>
                    <TextField id="name" name="name" label="รายการ*" variant="outlined" fullWidth onChange={handeleChange} />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="price" name="price" label="ราคา* (บาท)" variant="outlined" type="number" 
                    inputProps={{ min: "0", step: "1" }} fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="amount" name="amount" label="จำนวน" variant="outlined" type="number" defaultValue={1}
                    inputProps={{ min: "0", step: "1" }} fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                <Button size="large" variant="contained" color="primary" fullWidth onClick={onAdd}>
                    เพิ่ม
                </Button>
                </Grid>
            </Grid>
        </form>
    )
}
const PenddingSale = ()=>{
    const firestore = useFirestore()
    const dispatch = useDispatch()
    const baseRef = firestore.collection('pendingSale')
    const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
    const [sumOfSaleList,setSumOfSaleList] = useState(0)
    useEffect(()=>{
        const sum = rows.reduce((s,i)=>{
            return (i.price*i.amount)+s
        },0)
        setSumOfSaleList(sum)
        dispatch({
          type:'SALE_SET_SUM',
          value: sum,
        })
    },[rows])
    const handleDelete = (id)=>{
      baseRef.doc(id).delete()
    }
    const handleIncrement = (id,amount,f)=>{
      if((amount+f)>=1){
        baseRef.doc(id).update({
          amount: amount+f
        });
      }
    }
    return (
        <>
        <Grid container justify="space-between" spacing={1}>
            <Grid item>
                <Typography variant="h4" gutterBottom>
                ราคารวม :
                </Typography>
            </Grid>
            <Grid item >
                <Typography variant="h4" gutterBottom>
                {sumOfSaleList}
                </Typography>
            </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table size="small" aria-label="saleList">
            <TableHead>
              <TableRow>
                <TableCell>รายการ</TableCell>
                <TableCell align="right">จำนวน</TableCell>
                <TableCell align="right">ราคา</TableCell>
                <TableCell>รวม</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">
                  <IconButton aria-label="delete" onClick={()=>{handleIncrement(row.id,row.amount,-1)}} size="small">
                      <RemoveIcon fontSize="small" />
                    </IconButton>
                    <IconButton aria-label="delete" onClick={()=>{handleIncrement(row.id,row.amount,1)}} size="small">
                      <AddIcon fontSize="small" />
                    </IconButton>&nbsp;
                    {row.amount}
                    </TableCell>
                  <TableCell align="right">{row.price}</TableCell>
                  <TableCell align="right">
                    {row.price*row.amount}&nbsp;
                    <IconButton aria-label="delete" onClick={()=>{handleDelete(row.id)}} size="small">
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </>
      );
}
const SaleTools = ()=>{
  const dispatch = useDispatch()
  const handleClick = ()=>{
    dispatch({
      type:'SALE_SHOW_DIALOGPAY',
      value: true,
    })
  }
  return (
    <>
    <SaleType />
    <DialogPay />
    <Button size="large" variant="contained" color="primary" fullWidth onClick={handleClick}>
        ชำระเงิน
    </Button>
    </>
  )
}

const Sale = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            ขายสินค้า
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <FormInput />   
            </Grid>
            <Grid item xs={12} sm={4}>
                <PenddingSale />
                <br/>
                <SaleTools />
            </Grid>
            <Grid item xs={12} sm={8}>
                <QuickSale />
            </Grid>
        </Grid>
        </>
    )
}
export default Sale