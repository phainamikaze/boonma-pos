import React, { useState } from 'react';
import {
    Typography,
    InputLabel,
    Grid,
    FormControl,
    IconButton,
    OutlinedInput,
    InputAdornment,
} from '@material-ui/core'
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux'
import TableProducts from '../components/TableProducts'

const FormInput = ()=>{
    const dispatch = useDispatch()
    const keyword = useSelector(state=>state.product.keyword)
    const handeleChange = (e)=>{
      dispatch({
        type:"PRODUCTS_SET_KEYWORD",
        value:e.target.value
      })
    }
    return (
        <form >
          <FormControl fullWidth  variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">ค้นหาสินค้า</InputLabel>
            <OutlinedInput
              id="filter"
              value={keyword}
              onChange={handeleChange}
              autoFocus={true}
              autoComplete="no"
              startAdornment={<InputAdornment position="start"><SearchIcon/></InputAdornment>}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="clear"
                    onClick={()=>{dispatch({
                      type:"PRODUCTS_SET_KEYWORD",
                      value:""
                    })}}
                    edge="end"
                  >
                    <ClearIcon/>
                  </IconButton>
                </InputAdornment>
              }
              labelWidth={60}
            />
          </FormControl>
        </form>
    )
}

const Products = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            สินค้า
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <FormInput />   
            </Grid>
            <Grid item xs={12}>
                <TableProducts />
                <br/>
            </Grid>
        </Grid>
        </>
    )
}
export default Products