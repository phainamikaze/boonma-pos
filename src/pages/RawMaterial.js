import React, { useEffect,useState,useRef } from 'react';
import {
    Typography,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Grid,
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {useFirestoreCollectionData, useFirestore} from 'reactfire';
const FormInput = ()=>{
    const firestore = useFirestore();
    const inputFrom = useRef(null);
    const inputName = useRef(null);
    const baseRef = firestore.collection('rawMaterial');
    const [inputForm,setInputForm] = useState({})
    const handeleChange = (e)=>{
        setInputForm({
            ...inputForm,
            [e.target.name]: e.target.value
        })
    }
    const onAdd = () =>{
        baseRef.add(inputForm)
        .then(()=>{
            inputFrom.current.reset()
            inputName.current.focus()
        })
    }
    return (
        <form ref={inputFrom}>
            <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
                <Grid item xs={12} sm={10}>
                    <TextField ref={inputName} id="name" name="name" label="วัตถุดิบ*" variant="outlined" fullWidth onChange={handeleChange} autoFocus />
                </Grid>
                <Grid item xs={12} sm={2}>
                <Button size="large" variant="contained" color="primary" fullWidth onClick={onAdd}>
                    เพิ่ม
                </Button>
                </Grid>
            </Grid>
        </form>
    )
}
const ShowTable = ()=>{
    const firestore = useFirestore()
    const baseRef = firestore.collection('rawMaterial');
    const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
    const handleDelete = (id)=>{
        if(window.confirm('ต้องการลบ?')){
          baseRef.doc(id).delete()
          .then(()=>{
            
          })
        }
      }
    return (
        <TableContainer component={Paper}>
          <Table size="small" aria-label="PendongStock">
            <TableHead>
              <TableRow>
                <TableCell>รายการวัตถุดิบ</TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell component="th" scope="row" align="right">
                    <IconButton aria-label="delete" onClick={()=>handleDelete(row.id)}>
                        <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      );
}
const RawMaterial = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            วัตถุดิบ
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <FormInput />   
            </Grid>
            <Grid item xs={12} sm={12}>
                <ShowTable />
            </Grid>
        </Grid>
        </>
    )
}
export default RawMaterial