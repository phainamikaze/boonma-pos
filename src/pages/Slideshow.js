import React, { useState,useEffect } from 'react';
import MUIDataTable from "mui-datatables";
import {
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Grid,
  TextField,
  Dialog,
  Fab,
  Button,
  Slide,
  IconButton,
  LinearProgress,
  Typography,
  MenuItem,
  FormControl,
  Select,
  InputLabel,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import LockIcon from '@material-ui/icons/Lock';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import { red,yellow } from '@material-ui/core/colors';
import {useFirestoreCollectionData, useFirestore,useStorage,useFirestoreDocData,useStorageTask, SuspenseWithPerf} from 'reactfire'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const UploadProgress = ({ uploadTask, storageRef }) => {
  const classes = useStyles();
  const { bytesTransferred, totalBytes } = useStorageTask(
    uploadTask,
    storageRef
  );

  const percentComplete =
    Math.round(100 * (bytesTransferred / totalBytes)) + '%';

  return (
  <>
    <LinearProgress variant="determinate" className={classes.TextField} value={percentComplete} />
    <Typography variant="overline" display="block" gutterBottom>
    {percentComplete}
      </Typography>
  </>
  )
};
const useStyles = makeStyles((theme) => ({
  TextField: {
    marginTop: theme.spacing(2),
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(5),
    right: theme.spacing(5),
  },
}));
const EditDialog = ({open,id,onClose,tableMeta}) => {
  const classes = useStyles();
  const firestore = useFirestore()
  const baseRef = firestore.collection('slideshow')
  const storage = useStorage()
  const ref = baseRef.doc(id)
  const editItem = useFirestoreDocData(ref,{ idField: 'id' })
  const pcRef = firestore.collection('productCategory')
  const pc = useFirestoreCollectionData(pcRef, { idField: 'id' ,startWithValue:[] } )
  const [formEdit,setFormEdit] = useState({
    title:'',
    desc:'',
    order:1,
    disabled:false,
  })

  const [storageref, setStorageRef] = useState(null)
  const [uploadTask, setUploadTask] = useState(null);
  const [fullPathImg, setFullPathImg] = useState(null);
  const handleClose = ()=>{
    onClose()
  }
  const onChangeFile = (event)=>{
    const fileList = event.target.files;
    const fileToUpload = fileList[0];
    //const fileName = fileToUpload.name;
    const newRef = storage
      .ref('slideshow')
      .child(id);
      setStorageRef(newRef);
    const uploadTask = newRef.put(fileToUpload);
    uploadTask.then(() => {
      console.log('upload complete');
      setUploadTask(null);
      return storage.ref('slideshow').child(id).getDownloadURL()
    })
    .then(url=>{
      setFullPathImg(url)
    }).catch(()=>{
      setFullPathImg(null)
    })
    setUploadTask(uploadTask);
  }
  const handleDeleteImg = (e)=>{
    if(window.confirm('ลบรูปภาพ?')){
      storage
      .ref('slideshow')
      .child(id).delete()
      .then(()=>{
        setFullPathImg(null)
      })
    }
      
  }
  const handleSave = ()=>{
    console.log(formEdit)
    ref.update(formEdit).then(()=>{
      handleClose()
    })
    .catch((err)=>{
      console.log(err)
    })
  }
  const handleChange = (e)=>{
    if(e.target.name==='order'){
      setFormEdit({...formEdit,[e.target.name]:Number(e.target.value)})
    }else{
      setFormEdit({...formEdit,[e.target.name]:e.target.value})
    }
    
  }
  useEffect(()=>{
    setFormEdit({...editItem})
    storage.ref('slideshow').child(id).getDownloadURL()
    .then(url=>{
      setFullPathImg(url)
    }).catch(()=>{
      setFullPathImg(null)
    })
  },[editItem])
  return (
      <Dialog
        maxWidth="sm"
        fullWidth={true}
        open={open}
        TransitionComponent={Transition}
        keepMounted={false}
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">แก้ไข</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">

              <TextField id="product-order" label="ลำดับ" name="order" 
                value={formEdit.order} onChange={handleChange} variant="outlined" type="number" 
                fullWidth={true} className={classes.TextField}
                onFocus={(event) => event.target.select()}/>

              <TextField id="product-name" label="title" name="title" 
                value={formEdit.name} onChange={handleChange} variant="outlined" type="text" 
                fullWidth={true} className={classes.TextField} />

              <TextField id="product-desc" label="description" name="desc" 
                value={formEdit.desc} onChange={handleChange} variant="outlined" type="text" 
                fullWidth={true} className={classes.TextField} />

              {uploadTask ? 
              (<UploadProgress uploadTask={uploadTask} storageRef={storageref}  />):
              (
                <>
                {fullPathImg===null?(<TextField id="product-pic" label="รูปภาพ" variant="outlined" type="file" 
                  accept="image/png, image/jpeg" 
                  className={classes.TextField} 
                  fullWidth={true} 
                  onChange={onChangeFile}/>
                  ):(
                    <>
                    <img src={fullPathImg} style={{ width: '400px' }} className={classes.TextField}/>
                    <IconButton color="secondary" aria-label="delete" fontSize="large" component="span" onClick={handleDeleteImg}>
                      <DeleteIcon />
                    </IconButton>
                    </>
                  )}
                </>
              )
              }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <Button size="large" onClick={handleClose} variant="outlined" color="primary">
            ยกเลิก
          </Button>
          <Button size="large" fullWidth variant="contained" color="primary" onClick={handleSave}>
            บันทึก
          </Button>
        </DialogActions>
      </Dialog>
  );
}
const AddDialog = ({open,onClose}) => {
  const classes = useStyles();
  const firestore = useFirestore()
  const baseRef = firestore.collection('slideshow')
  const storage = useStorage()
  const [id,setID] = useState(null)
  const initFormEdit = {
    title:'',
    desc:'',
    order:1,
    disabled:false,
  }
  const [formEdit,setFormEdit] = useState(initFormEdit)
  const [ReID,setReID] = useState(false)
  useEffect(()=>{
    const ref = baseRef.doc()
    setID(ref.id)
  },[ReID])
  const [storageref, setStorageRef] = useState(null)
  const [uploadTask, setUploadTask] = useState(null);
  const [fullPathImg, setFullPathImg] = useState(null);
  const handleClose = ()=>{
    onClose()
  }
  const onChangeFile = (event)=>{
    const fileList = event.target.files;
    const fileToUpload = fileList[0];
    //const fileName = fileToUpload.name;
    const newRef = storage
      .ref('slideshow')
      .child(id);
      setStorageRef(newRef);
    const uploadTask = newRef.put(fileToUpload);
    uploadTask.then(() => {
      console.log('upload complete');
      setUploadTask(null);
      return storage.ref('slideshow').child(id).getDownloadURL()
    })
    .then(url=>{
      setFullPathImg(url)
    }).catch(()=>{
      setFullPathImg(null)
    })
    setUploadTask(uploadTask);
  }
  const handleDeleteImg = (e)=>{
    if(window.confirm('ลบรูปภาพ?')){
      storage
      .ref('slideshow')
      .child(id).delete()
      .then(()=>{
        setFullPathImg(null)
      })
    } 
  }
  const handleSave = ()=>{
    baseRef.doc(id).set(formEdit).then(()=>{
      setFormEdit(initFormEdit)
      setFullPathImg(null)
      setReID(!ReID)
      handleClose()
    })
    .catch((err)=>{
      console.log(err)
    })
  }
  const handleChange = (e)=>{
    if(e.target.name==='order'){
      setFormEdit({...formEdit,[e.target.name]:Number(e.target.value)})
    }else{
      setFormEdit({...formEdit,[e.target.name]:e.target.value})
    }
    
  }
  return (
      <Dialog
        maxWidth="sm"
        fullWidth={true}
        open={open}
        TransitionComponent={Transition}
        keepMounted={false}
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">เพิ่มใหม่</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">

              <TextField id="product-order" label="ลำดับ" name="order" 
                value={formEdit.order} onChange={handleChange} variant="outlined" type="number" 
                fullWidth={true} className={classes.TextField}
                onFocus={(event) => event.target.select()}/>

              <TextField id="product-title" label="title" name="title" 
                value={formEdit.title} onChange={handleChange} variant="outlined" type="text" 
                fullWidth={true} className={classes.TextField} autoFocus={true}/>

              <TextField id="product-desc" label="description" name="desc" 
                value={formEdit.desc} onChange={handleChange} variant="outlined" type="text" 
                fullWidth={true} className={classes.TextField} />

              {uploadTask ? 
              (<UploadProgress uploadTask={uploadTask} storageRef={storageref}  />):
              (
                <>
                {fullPathImg===null?(<TextField id="product-pic" label="รูปภาพ" variant="outlined" type="file" 
                  accept="image/png, image/jpeg" 
                  className={classes.TextField} 
                  fullWidth={true} 
                  onChange={onChangeFile}/>
                  ):(
                    <>
                    <img src={fullPathImg} style={{ width: '400px' }} className={classes.TextField}/>
                    <IconButton color="secondary" aria-label="delete" fontSize="large" component="span" onClick={handleDeleteImg}>
                      <DeleteIcon />
                    </IconButton>
                    </>
                  )}
                </>
              )
              }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <Button size="large" onClick={handleClose} variant="outlined" color="primary">
            ยกเลิก
          </Button>
          <Button size="large" fullWidth variant="contained" color="primary" onClick={handleSave}>
            บันทึก
          </Button>
        </DialogActions>
      </Dialog>
  );
}
const AddButton = ()=>{
  const classes = useStyles();
  const [open,setOpen] = useState(false)
  return (<>
    <Fab aria-label="Add" className={classes.fab} color="primary" onClick={()=>setOpen(true)}>
    <AddIcon />
  </Fab>
  <AddDialog open={open} onClose={()=>setOpen(false)}/>
  </>)
}
const ItemActions = ({id,tableMeta})=>{
  const firestore = useFirestore()
  const storage = useStorage()
  const baseRef = firestore.collection('slideshow')
  const [open,setOpen] = useState(false)
  const handleEdit = ()=>{
    setOpen(true)
  }
  const handleDelete = ()=>{
    if(window.confirm(`ยืนยันการลบ ?`)){
      baseRef.doc(id).delete()
      storage
      .ref('slideshow')
      .child(id).delete()
    }
  }
  const handleRecommend = () =>{
    baseRef.doc(id).update({recommend: !tableMeta.rowData[1]})
  }
  const handleLock = () =>{
    baseRef.doc(id).update({disabled: !tableMeta.rowData[0]})
  }
  return (<>
  <IconButton onClick={handleLock}>
    <LockIcon style={{ color: tableMeta.rowData[0]===true?red['A700']:null }}/>
  </IconButton>
  <IconButton onClick={handleEdit}>
    <EditIcon/>
  </IconButton>
  <IconButton onClick={handleDelete}>
    <DeleteIcon/>
  </IconButton><EditDialog tableMeta={tableMeta} open={open} id={id} onClose={()=>setOpen(false)} /> </>)
}
const ColCategory = ({value}) =>{
  const firestore = useFirestore()
  const pcRef = firestore.collection('productCategory')
  const pcRows = useFirestoreCollectionData(pcRef, { idField: 'id' ,startWithValue:[] })
  const [result,setResult] = useState(null)
  useEffect(()=>{
    pcRows.some((doc)=>{
      if(doc.id===value){
        setResult(doc.name)
        return true
      }else{
        return false
      }
    })
  },[pcRows,value])
  return result
}
const Product =()=>{
  const columns = [
    {
      name: "disabled",
      options: {
        display: 'excluded'
      }
    },
    {
      name: "recommend",
      options: {
        display: 'excluded'
      }
    },
    {
      name: "title",
      label: "title",
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: "desc",
      label: "description",
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: "imgUrl",
      label: "รูป",
      options: {
        filter: true,
        sort: false,
        customBodyRender:(value, tableMeta, updateValue) => {
          return <img loading="lazy" src={value} style={{ height: '200px' }}/>
        }
      }
    },
    {
      name: "order",
      label: "เรียงลำดับ",
      options: {
        filter: true,
        sort: true,
      }
    },
    {
      name: "id",
      label: "แก้ไข",
      options: {
        filter: true,
        sort: true,
        customBodyRender:(value, tableMeta, updateValue) => {
          return <ItemActions id={value} tableMeta={tableMeta}/>
        }
      }
    },
  ]
  const options = {
    download: false,
    print: false,
    filter: false,
    selectableRows:'none',
  }
  const storage = useStorage()
  const firestore = useFirestore()
  const baseRef = firestore.collection('slideshow') //.orderBy('order')
  const rows = useFirestoreCollectionData(baseRef, { idField: 'id' ,startWithValue:[] })
  const [data,setData] = useState([])

  useEffect(()=>{
    const r = rows.map(async i=>{
      try{
        const url = await storage.ref('slideshow').child(i.id).getDownloadURL()
        return {...i,imgUrl:url}
      }catch(e){
        return {...i,imgUrl:null}
      }
    })
    Promise.all(r)
    .then(result=>{
      setData(result)
      console.log(result)
    })
    
  },[rows])

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MUIDataTable 
            title={`slideshow`} 
            data={data} 
            columns={columns} 
            options={options} 
          />
        </Grid>
      </Grid>
      <AddButton />
    </>
    )
}
export default Product