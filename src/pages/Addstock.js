import React, { useEffect,useState,useRef } from 'react';
import {
    Typography,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Grid,
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import debounce from '../debounce'
import {useFirestoreCollectionData, useFirestore} from 'reactfire';
const FormInput = ()=>{
    const firestore = useFirestore();
    const [code,setCode] = useState('')
    const refFrom = useRef(null);
    const refInputName = useRef(null);
    const refInputPrice = useRef(null);
    const refInputCode = useRef(null)
    const baseRef = firestore.collection('stock');
    const baseRef2 = firestore.collection('pendingStock');
    const [inputForm,setInputForm] = useState({})
    const handeleChange = (e)=>{
        setInputForm({
            ...inputForm,
            [e.target.name]: isNaN(e.target.value)?e.target.value:Number(e.target.value)
        })
    }
    const handeleScan = debounce(async(v) => {
        setCode(v)
        try{
            let data = undefined
            const baseRefResult = await baseRef.doc(v).get()
            if(baseRefResult.data()===undefined){
                const baseRef2Result = await baseRef2.where('itemId','==',v).get()
                if(baseRef2Result.docs[0]===undefined){
                    // not found
                    console.log('not found')
                    refInputName.current.focus()
                }else{
                    data = baseRef2Result.docs[0].data()
                }
            }else{
                data = baseRefResult.data()
            }
            if(data !== undefined){
                baseRef2.add({
                    ...data,
                    itemId:code
                }).then(()=>{
                    refFrom.current.reset()
                })
            }else{
                console.log('not found2')
                refInputName.current.focus()
            }
            
        }catch(err){
            console.log(`err`)
            console.log(err)
        }
    }, 500)
    const onAdd = () =>{
        baseRef2.add({
            ...inputForm,
            itemId:code
        }).then(()=>{
            refFrom.current.reset()
        })
    }
    return (
        <form ref={refFrom} >
            <Grid container spacing={1} direction="row" justify="flex-start" alignItems="center">
                <Grid item xs={12} sm={2}>
                    <TextField autoComplete='off' ref={refInputCode} id="code" name="code" label="code" onFocus={(event) => event.target.select()}
                    variant="outlined" fullWidth onChange={(e)=>handeleScan(e.target.value)} autoFocus />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField ref={refInputName} id="name" name="name" label="รายการ*" variant="outlined" fullWidth onChange={handeleChange} />
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField ref={refInputPrice} id="price" name="price" label="ราคา* (บาท)" variant="outlined" type="number" inputProps={{ min: "0", step: "1" }} fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="amount" name="amount" label="จำนวน" variant="outlined" fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                    <TextField id="unit" name="unit" label="หน่วย (กรัม,ฟอง,มิลลิลิตร)" variant="outlined" fullWidth onChange={handeleChange}/>
                </Grid>
                <Grid item xs={12} sm={2}>
                <Button size="large" variant="contained" color="primary" fullWidth onClick={onAdd}>
                    เพิ่ม
                </Button>
                </Grid>
            </Grid>
            {/* <FormControl variant="outlined" >
                <InputLabel id="amount">จำนวน*</InputLabel>
                <Select
                    labelId="amount"
                    id="amount"
                    value={1}
                    onChange={()=>{}}
                    label="จำนวน"
                    >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={6}>6</MenuItem>
                </Select>
            </FormControl> */}
        </form>
    )
}
const PendongStock = ()=>{
    const firestore = useFirestore()
    const baseRef = firestore.collection('pendingStock')
    const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
    const handleDelete = (id)=>{
        if(window.confirm('ต้องการลบ?')){
          baseRef.doc(id).delete()
          .then(()=>{
            
          })
        }
      }
    return (
        <>
        <TableContainer component={Paper}>
          <Table size="small" aria-label="PendongStock">
            <TableHead>
              <TableRow>
                <TableCell>รายการ</TableCell>
                <TableCell align="right">ราคา&nbsp;(บาท)</TableCell>
                <TableCell align="right">จำนวน</TableCell>
                <TableCell align="right">หน่วย</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.price}</TableCell>
                  <TableCell align="right">{row.amount}</TableCell>
                  <TableCell align="right">{row.unit}</TableCell>
                  <TableCell align="center">
                    <IconButton aria-label="delete" onClick={()=>handleDelete(row.id)}>
                    <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Button size="large" variant="contained" color="primary" fullWidth onClick={()=>{}}>
            บันทึก
        </Button>
        </>
      );
}
const Addstock = ()=>{
    return (
        <>
        <Typography variant="h6" gutterBottom>
            เพิ่มสต๊อก
        </Typography>
        
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <FormInput />   
            </Grid>
            <Grid item xs={12} sm={6}>
            </Grid>
            <Grid item xs={12} sm={6}>
                <PendongStock />
            </Grid>
        </Grid>
        </>
    )
}
export default Addstock