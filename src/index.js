import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { FirebaseAppProvider } from 'reactfire';
const firebaseConfig = {
  apiKey: "AIzaSyBhQOwCuodzABXshVHsUI-cwHl3YHNTbFk",
  authDomain: "boonma-pos.firebaseapp.com",
  databaseURL: "https://boonma-pos.firebaseio.com",
  projectId: "boonma-pos",
  storageBucket: "boonma-pos.appspot.com",
  messagingSenderId: "985624439469",
  appId: "1:985624439469:web:d4ff48de645127ad5a7424",
  measurementId: "G-5S2R0W8FXZ"
};
ReactDOM.render(
  <React.StrictMode>
    <FirebaseAppProvider firebaseConfig={firebaseConfig}>
      <App />
    </FirebaseAppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);


