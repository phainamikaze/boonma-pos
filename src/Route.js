import React from 'react';
import Layout from './Layout';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import {
  AuthCheck,
} from 'reactfire';

import Test from './pages/test'
import Sale from './pages/Sale'
import Signin from './pages/Signin'
import Addstock from './pages/Addstock'
import SaleReport from './pages/SaleReport'
import SaleSumary from './pages/SaleSumary'
import Products from './pages/Products'
import Preorder from './pages/Preorder'
import PreorderList from './pages/PreorderList'
import PreorderDelivery from './pages/PreorderDelivery'
import Stock from './pages/Stock'
import RawMaterial from './pages/RawMaterial'
import PreorderEdit from './pages/PreorderEdit'
import ProductCategory from './pages/ProductCategory'
import Product from './pages/Product'
import Slideshow from './pages/Slideshow'
export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login"><Signin/></Route>
        <Route path="/slideshow"><AuthCheck fallback={<Signin/>}><Layout component={<Slideshow/>}/></AuthCheck></Route>
        <Route path="/sale"><AuthCheck fallback={<Signin/>}><Layout component={<Sale/>}/></AuthCheck></Route>
        <Route path="/productcategory"><AuthCheck fallback={<Signin/>}><Layout component={<ProductCategory/>}/></AuthCheck></Route>
        <Route path="/product"><AuthCheck fallback={<Signin/>}><Layout component={<Product/>}/></AuthCheck></Route>
        <Route path="/products"><AuthCheck fallback={<Signin/>}><Layout component={<Products/>}/></AuthCheck></Route>
        <Route path="/preorder/:preOrderId"><AuthCheck fallback={<Signin/>}><Layout component={<PreorderEdit/>}/></AuthCheck></Route>
        <Route path="/preorder"><AuthCheck fallback={<Signin/>}><Layout component={<Preorder/>}/></AuthCheck></Route>
        <Route path="/preorderlist"><AuthCheck fallback={<Signin/>}><Layout component={<PreorderList/>}/></AuthCheck></Route>
        <Route path="/preorderdelivery"><AuthCheck fallback={<Signin/>}><Layout component={<PreorderDelivery/>}/></AuthCheck></Route>
        <Route path="/salereport"><AuthCheck fallback={<Signin/>}><Layout component={<SaleReport/>}/></AuthCheck></Route>
        <Route path="/salesumary"><AuthCheck fallback={<Signin/>}><Layout component={<SaleSumary/>}/></AuthCheck></Route>
        <Route path="/addstock"><AuthCheck fallback={<Signin/>}><Layout component={<Addstock/>}/></AuthCheck></Route>
        <Route path="/stock"><AuthCheck fallback={<Signin/>}><Layout component={<Stock/>}/></AuthCheck></Route>
        <Route path="/rawmaterial"><AuthCheck fallback={<Signin/>}><Layout component={<RawMaterial/>}/></AuthCheck></Route>
        <Route path="/"><AuthCheck fallback={<Signin/>}><Layout component={<PreorderDelivery/>} /></AuthCheck></Route>
        
      </Switch>
    </Router>
  );
}