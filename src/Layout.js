import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import ReceiptIcon from '@material-ui/icons/Receipt';
import BarChartIcon from '@material-ui/icons/BarChart';
import LocalMallIcon from '@material-ui/icons/LocalMall';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { useUser, useAuth } from 'reactfire';
import {
  useHistory,
} from "react-router-dom";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  usericon :{
    marginLeft: 'auto',
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));


function Layout(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const history = useHistory();
  const user = useUser();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const signOut = auth =>
    auth
      .signOut()
      .then(() => history.push("/login"));

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
          <ListItem component={Link} to="/sale" button key="sale">
            <ListItemIcon><ShoppingCartIcon /></ListItemIcon>
            <ListItemText primary={'ขายสินค้า'} />
          </ListItem>
          <ListItem component={Link} to="/salereport" button key="salereport">
            <ListItemIcon><ReceiptIcon /></ListItemIcon>
            <ListItemText primary={'รายงานการขาย'} />
          </ListItem>
          <ListItem component={Link} to="/salesumary" button key="salesumary">
            <ListItemIcon><BarChartIcon /></ListItemIcon>
            <ListItemText primary={'ยอดขาย'} />
          </ListItem>
          <ListItem component={Link} to="/product" button key="product">
            <ListItemIcon><FastfoodIcon /></ListItemIcon>
            <ListItemText primary={'สินค้า'} />
          </ListItem>
          <ListItem component={Link} to="/productcategory" button key="productcategory">
            <ListItemIcon><FastfoodIcon /></ListItemIcon>
            <ListItemText primary={'หมวดหมู่สินค้า'} />
          </ListItem>
      </List>
      <Divider />
      <List>
          <ListItem component={Link} to="/slideshow" button key="slideshow">
            <ListItemIcon><LocalMallIcon /></ListItemIcon>
            <ListItemText primary={'slideshow'} />
          </ListItem>
      </List>
      <Divider />
      <List>
          <ListItem component={Link} to="/preorder" button key="preorder">
            <ListItemIcon><LocalMallIcon /></ListItemIcon>
            <ListItemText primary={'พรีออเดอร์'} />
          </ListItem>
          <ListItem component={Link} to="/preorderlist" button key="preorder">
            <ListItemIcon><LocalMallIcon /></ListItemIcon>
            <ListItemText primary={'รายการออเดอร์'} />
          </ListItem>
          <ListItem component={Link} to="/preorderdelivery" button key="preorder">
            <ListItemIcon><LocalMallIcon /></ListItemIcon>
            <ListItemText primary={'รายการที่ต้องจัดส่ง'} />
          </ListItem>
      </List>
      <Divider />
      <List>
          <ListItem component={Link} to="/addstock" button key="addstock">
            <ListItemIcon><InboxIcon /></ListItemIcon>
            <ListItemText primary={'เพิ่มสต๊อก'} />
          </ListItem>
          <ListItem component={Link} to="/rawmaterial" button key="rawmaterial">
            <ListItemIcon><InboxIcon /></ListItemIcon>
            <ListItemText primary={'วัตถุดิบ'} />
          </ListItem>
      </List>
    </div>
  );
  const auth = useAuth();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            บุญมาคาเฟ่
          </Typography>
          <div className={classes.usericon}>
              {user.displayName}
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={() => signOut(auth)}>ออกจากระบบ</MenuItem>
              </Menu>
            </div>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {props.component}
      </main>
    </div>
  );
}

Layout.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.any,
};

export default Layout;