import React from 'react';
import Radio from '@material-ui/core/Radio';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { useDispatch,useSelector } from 'react-redux'
export default function RadioButtonsGroup() {
  const dispatch = useDispatch()
  const value = useSelector(state=>state.sale.saleType)
  const seller = useSelector(state=>state.sale.seller)
  const handleChange = (event) => {
    localStorage.setItem('sale_saleType',event.target.value);
    dispatch({
      type:'SALE_SET_SALETYPE',
      value:event.target.value
    })
  };
  const handleChange2 = (event) => {
    localStorage.setItem('sale_seller',event.target.value);
    dispatch({
      type:'SALE_SET_SELLER',
      value:event.target.value
    })
  };
  return (
    <>
    <FormControl component="fieldset">
      <FormLabel component="legend">ขายผ่าน</FormLabel>
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
        <FormControlLabel value="store" control={<Radio />} label="หน้าร้าน" />
        <FormControlLabel value="grab" control={<Radio />} label="grab" />
        <FormControlLabel value="facebook" control={<Radio />} label="facebook" />
      </RadioGroup>
    </FormControl>
    <FormControl variant="outlined" >
    <InputLabel id="demo-simple-select-outlined-label">ผู้ขาย</InputLabel>
    <Select autoWidth
      labelId="demo-simple-select-outlined-label"
      id="demo-simple-select-outlined"
      value={seller}
      onChange={handleChange2}
      label="ผู้ขาย"
    >
      <MenuItem value={"ไผ่"}>ไผ่</MenuItem>
      <MenuItem value={"ปิงปิง"}>ปิงปิง</MenuItem>
      <MenuItem value={"แม่"}>แม่</MenuItem>
    </Select>
  </FormControl>
  </>
  );
}