import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import Typography from '@material-ui/core/Typography';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import moment from 'moment'
import {
    useFirestoreCollectionData,
    useFirestore
  } from 'reactfire';
import {
    yellow,
} from '@material-ui/core/colors';
import {useHistory} from 'react-router-dom'
import {useSelector} from 'react-redux'
import TablePreorderPinList from './TablePreorderPinList'
import PreorderToolBar from './PreorderToolBar'
import TablePreorderSummary from './TablePreorderSummary'
import { DomainPropTypes } from '@material-ui/pickers/constants/prop-types';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  '1':{
    backgroundColor:yellow[500]
  }
});

const TablePreoderList = () => {
  const classes = useStyles();
  const history = useHistory()
  const firestore = useFirestore();
  const d = useSelector(state=>state.preorder.shippingDate)
  const d1 = new Date(`${d.format('YYYY-MM-DD')} 00:00:00`)
  const d2 = new Date(`${d.format('YYYY-MM-DD')} 23:59:59`)
  const baseRef = firestore.collection('preorder');
  const query = baseRef.where('shippingDate','>=',d1).where('shippingDate','<=',d2).orderBy('shippingDate','desc');
  const preorderList = useFirestoreCollectionData(query, { idField: 'id' });
  const handleDelete = (id)=>{
    if(window.confirm('ต้องการลบ?')){
      baseRef.doc(id).delete()
      .then(()=>{
        
      })
    }
  }
  const handleEdit = (id) =>{
    history.push(`/preorder/${id}`);
  }
  return (
    <>
    <Grid container spacing={3}>
      <Grid item xs={12}>
          <PreorderToolBar />
      </Grid>
      <Grid item xs={12}>
          <TablePreorderSummary items={preorderList} />
      </Grid>
    </Grid>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>กำนดส่ง</TableCell>
            <TableCell>ลูกค้า</TableCell>
            <TableCell>เบอร์โทร</TableCell>
            <TableCell>รายละเอียด</TableCell>
            <TableCell>รายการ</TableCell>
            <TableCell align="right">ราคา</TableCell>
            <TableCell align="right">ค่าส่ง</TableCell>
            <TableCell align="right">ราคารวม</TableCell>
            <TableCell align="center">สถานะ </TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {preorderList.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {moment(row.shippingDate.seconds*1000).calendar()}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.cname}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.cphone}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.cremark}
              </TableCell>
              <TableCell align="right"><TablePreorderPinList items={row.items} /></TableCell>
              <TableCell align="right">{row.shipping?row.net-row.shippingCost:row.net}</TableCell>
              <TableCell align="right">{row.shipping?row.shippingCost:'ฟรี'}</TableCell>
              <TableCell align="right">
              <Typography variant="h5" gutterBottom>
                  {row.net}
                  </Typography>
              </TableCell>
              <TableCell align="center">{
              (row.status===1)?<Chip size="medium" style={{backgroundColor:'#ff8000'}} label="รับออเดอร์" className={`${row.status}`} />:
              (row.status===0)?<Chip size="medium" style={{backgroundColor:'green'}} label="จัดส่งแล้ว" className={`${row.status}`} />:
              'unknown'}</TableCell>
            
              <TableCell>
              < IconButton aria-label="delete" onClick={()=>handleEdit(row.id)}>
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" onClick={()=>handleDelete(row.id)}>
                  <DeleteIcon />
                </IconButton>
              </TableCell>
          </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
  );
}
export default TablePreoderList