import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import { Grid } from '@material-ui/core'
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import LinearProgress from '@material-ui/core/LinearProgress';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import {useFirestoreDocData, useFirestore,SuspenseWithPerf,useStorage,useStorageTask,StorageImage} from 'reactfire'
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  TextField: {
    marginTop: theme.spacing(2),
  },
}))
const UploadProgress = ({ uploadTask, storageRef }) => {
  const classes = useStyles();
  const { bytesTransferred, totalBytes } = useStorageTask(
    uploadTask,
    storageRef
  );

  const percentComplete =
    Math.round(100 * (bytesTransferred / totalBytes)) + '%';

  return (
  <>
    <LinearProgress variant="determinate" className={classes.TextField} value={percentComplete} />
    <Typography variant="overline" display="block" gutterBottom>
    {percentComplete}
      </Typography>
  </>
  )
};

export default function DialogProduct() {
  const classes = useStyles();
  const firestore = useFirestore()
  const dispatch = useDispatch()
  const baseRef = firestore.collection('products')
  const storage = useStorage();
  const open = useSelector(state=>state.product.editDialog)
  const editID = useSelector(state=>state.product.id) || 'notset'
  const ref = baseRef.doc(editID)
  const editItem = useFirestoreDocData(ref,{ idField: 'id' })
  const [name,setName] = useState("")
  const [price,setPrice] = useState("")
  const [code,setCode] = useState("aa")
  const [storageref, setStorageRef] = useState(null)
  const [uploadTask, setUploadTask] = useState(null);
  const [fullPathImg, setFullPathImg] = useState(null);
  const handleClose = ()=>{
    dispatch({
      type:'PRODUCTS_SET_EDITDIALOG',
      value:false,
      id:null,
    })
  }
  const onChangeFile = (event)=>{
    const fileList = event.target.files;
    const fileToUpload = fileList[0];
    //const fileName = fileToUpload.name;
    const newRef = storage
      .ref('images')
      .child(editID);
      setStorageRef(newRef);
    const uploadTask = newRef.put(fileToUpload);
    uploadTask.then(() => {
      console.log('upload complete');
      setUploadTask(null);
      return storage.ref('images').child(editID).getDownloadURL()
    })
    .then(url=>{
      setFullPathImg(url)
    }).catch(()=>{
      setFullPathImg(null)
    })
    setUploadTask(uploadTask);
  }
  const handleDeleteImg = (e)=>{
    if(window.confirm('ลบรูปภาพ?')){
      storage
      .ref('images')
      .child(editID).delete()
      .then(()=>{
        setFullPathImg(null)
      })
    }
      
  }
  const handleSave = ()=>{
    ref.update({
      name:name,
      code:code,
      price:price,
    }).then(()=>{
      handleClose()
    })
    .catch((err)=>{
      console.log(err)
    })
  }
  const handleChange = (e)=>{
    if(e.target.name==='name'){
      setName(e.target.value)
    }
    else if(e.target.name==='code'){
      setCode(e.target.value)
    }
    else if(e.target.name==='price'){
      setPrice(e.target.value)
    }
  }
  useEffect(()=>{
    setName(editItem.name)
    setCode(editItem.code)
    setPrice(editItem.price)
    storage.ref('images').child(editID).getDownloadURL()
    .then(url=>{
      setFullPathImg(url)
    }).catch(()=>{
      setFullPathImg(null)
    })
  },[editItem])
  return (
      <Dialog
        maxWidth="sm"
        fullWidth={true}
        open={open}
        TransitionComponent={Transition}
        keepMounted={false}
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">แก้ไข</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <SuspenseWithPerf
              fallback="waiting for progress..."
              traceId="dialog-products"
            >
              <TextField id="product-barcode" label="code" name="code"  value={code} onChange={handleChange} variant="outlined" type="text" fullWidth={true} className={classes.TextField} autoFocus={true}/>
              <TextField id="product-name" label="ชื่อสินค้า" name="name" value={name} onChange={handleChange} variant="outlined" type="text" fullWidth={true} className={classes.TextField} />
              <TextField id="product-price" label="ราคา" name="price" value={price} onChange={handleChange} variant="outlined" type="number" fullWidth={true} className={classes.TextField}/>
              {uploadTask ? 
              (<UploadProgress uploadTask={uploadTask} storageRef={storageref}  />):
              (
                <>
                {fullPathImg===null?(<TextField id="product-pic" label="รูปภาพ" variant="outlined" type="file" 
                  accept="image/png, image/jpeg" 
                  className={classes.TextField} 
                  fullWidth={true} 
                  onChange={onChangeFile}/>
                  ):(
                    <>
                    <img src={fullPathImg} style={{ width: '400px' }} className={classes.TextField}/>
                    <IconButton color="secondary" aria-label="delete" fontSize="large" component="span" onClick={handleDeleteImg}>
                      <DeleteIcon />
                    </IconButton>
                    </>
                  )}
                </>
              )
              }
            </SuspenseWithPerf>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <Button size="large" onClick={handleClose} variant="outlined" color="primary">
            ยกเลิก
          </Button>
          <Button size="large" fullWidth variant="contained" color="primary" onClick={handleSave}>
            บันทึก
          </Button>
        </DialogActions>
      </Dialog>
  );
}
