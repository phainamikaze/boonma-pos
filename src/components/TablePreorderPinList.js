import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    maxHeight: 300,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
  item: {
   paddingLeft:0
  }
}));

const TablePreorderPinList = ({items}) => {
  const classes = useStyles();

  return (
    <List className={classes.root} subheader={<li />}>

      <li key={`section-1`} className={classes.listSection}>
        <ul className={classes.ul}>
          {/* <ListSubheader></ListSubheader> */}
          {items.map((item) => {
            if('amount' in item && item.amount>0){
              return (<ListItem key={`item-1-${item}`} className={classes.item}>
                <ListItemText primary={`${item.amount} x ${item.name}`} />
              </ListItem>)
            }else{
              return null
            }
          }
          )}
        </ul>
      </li>

    </List>
  );
}
export default TablePreorderPinList