import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import Typography from '@material-ui/core/Typography';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import moment from 'moment';
import CheckIcon from '@material-ui/icons/Check';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import IconButton from '@material-ui/core/IconButton';
import CallIcon from '@material-ui/icons/Call';
import FacebookIcon from '@material-ui/icons/Facebook';
import { v4 as uuidv4 } from 'uuid';
import {
    useFirestoreCollectionData,
    useFirestore,
  } from 'reactfire';
import {
    yellow,
} from '@material-ui/core/colors';
import TablePreorderPinList from './TablePreorderPinList'
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  '1':{
    backgroundColor:yellow[500]
  }
});

const TablePreorderDelivery = () => {
  const classes = useStyles();
  const firestore = useFirestore();
  const baseRef = firestore.collection('preorder');
  const query = baseRef.where("status",">",0).orderBy('status').orderBy('shippingDate')
  const preorderList = useFirestoreCollectionData(query, { idField: 'id' });
  const handleOk = (id)=>{
    if(window.confirm('ยืนยันว่าจัดส่งเรียบร้อยแล้ว?')){
      baseRef.doc(id).update({status:0,deliveredAt:new Date()})
      .then(()=>{
        return baseRef.doc(id).get()
      })
      .then((result)=>{
        console.log(result.get('items'))
        // const txid = uuidv4();
        // const saveItem = pendingSale.map(item=>{
        //   return baseRef2.add({
        //     amount:item.amount,
        //     name:item.name,
        //     price:item.price,
        //     productId:item.id,
        //     saleType:saleType,
        //     txid:txid,
        //     seller:seller,
        //     timestamp: new Date(),
        //   })
        // })
      })
    }
  }
  
  const handleMap = (url)=>{
    window.open(url,'_blank');
  }
  const handleCall = (phonenumber)=>{
    window.open(`tel:[${phonenumber}]`);
  }
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>กำหนดส่ง</TableCell>
            <TableCell>รายละเอียด</TableCell>
            <TableCell align="center">พิกัด </TableCell>
            <TableCell align="center">Facebook </TableCell>
            <TableCell>เบอร์โทร</TableCell>
            <TableCell></TableCell>
            <TableCell>รายการ</TableCell>
            <TableCell align="right">ราคา ( * รวมค่าส่ง)</TableCell>
            
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {preorderList.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {moment(row.shippingDate.seconds*1000).calendar()}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.cname}<br/>{row.cremark}<br/>{row.cphone}
              </TableCell>
              <TableCell align="center">
                {row.clocation===''?null:
                <IconButton aria-label="delete" onClick={()=>handleMap(row.clocation)}>
                    <LocationOnIcon />
                </IconButton>}
              </TableCell>
              <TableCell align="center">
                {row.cfb===''?null:
                <IconButton aria-label="delete" onClick={()=>handleMap(row.cfb)}>
                    <FacebookIcon />
                </IconButton>}
              </TableCell>
              <TableCell component="th" scope="row">
                {row.cphone===''?null:
                <IconButton aria-label="delete" onClick={()=>handleCall(row.cphone)}>
                    <CallIcon />
                </IconButton>}
              </TableCell>
              <TableCell component="th" scope="row">
                
              </TableCell>
              <TableCell align="right"><TablePreorderPinList items={row.items} /></TableCell>
              <TableCell align="right">
              <Typography variant="h6" gutterBottom>
                  {row.net}{row.shipping?'*':''}
                  </Typography>
              </TableCell>
              
              <TableCell align="right">
                <IconButton aria-label="delete" onClick={()=>handleOk(row.id)}>
                  <CheckIcon />
                </IconButton>
              </TableCell>
          </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
export default TablePreorderDelivery