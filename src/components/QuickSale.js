import React, { useState,useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,

} from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
// const images = [
//   {
//     url: TestImage,
//     title: 'Breakfast',
//     width: '33%',
//   },
//   {
//     url: TestImage,
//     title: 'Burgers',
//     width: '33%',
//   },
//   {
//     url: TestImage,
//     title: 'Camera',
//     width: '33%',
//   },
//   {
//     url: TestImage,
//     title: 'Camera',
//     width: '33%',
//   },
// ];
import {useFirestoreCollectionData, useFirestore,useStorage} from 'reactfire'
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    minWidth: 300,
    width: '100%',
  },
  image: {
    position: 'relative',
    height: 200,
    [theme.breakpoints.down('xs')]: {
      width: '100% !important', // Overrides inline-style
      height: 100,
    },
    '&:hover, &$focusVisible': {
      zIndex: 1,
      '& $imageBackdrop': {
        opacity: 0.15,
      },
      '& $imageMarked': {
        opacity: 0,
      },
      '& $imageTitle': {
        border: '4px solid currentColor',
      },
    },
  },
  filter:{
    padding: `0px 0px ${theme.spacing(3)}px 0px`
  },
  focusVisible: {},
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${theme.spacing(1) + 6}px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
}));

export default function ButtonBases() {
  const classes = useStyles();
  const firestore = useFirestore()
  const storage = useStorage()
  const baseRef = firestore.collection('products')
  const baseRef2 = firestore.collection('pendingSale')
  const [keyword,setKeyword] = useState('')
  const [images,setimages] = useState([])
  const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
  useEffect(()=>{
    const r = rows.map(async i=>{
      try{
        const url = await storage.ref('images').child(i.id).getDownloadURL()
        return {...i,img:url}
      }catch(e){
        return {...i,img:null}
      }
    })
    Promise.all(r)
    .then(result=>{
      setimages(result)
    })
  },[rows])
  const handleAdd = (item) =>{
    console.log(item)
    baseRef2.doc(item.id).set({
      amount: 1,
      name: item.name,
      price: item.price,
    })
  }

  return (
    <>
    <FormControl fullWidth className={classes.filter} variant="outlined">
      <InputLabel htmlFor="outlined-adornment-amount">ค้นหาสินค้า</InputLabel>
      <OutlinedInput
        id="filter"
        value={keyword}
        onChange={(e)=>{setKeyword(e.target.value)}}
        startAdornment={<InputAdornment position="start"><SearchIcon/></InputAdornment>}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="clear"
              onClick={()=>{setKeyword('')}}
              edge="end"
            >
              <ClearIcon/>
            </IconButton>
          </InputAdornment>
        }
        labelWidth={60}
      />
    </FormControl>
    <div className={classes.root}>
      {images && images.filter((item)=>{
        if(keyword===''){
          return true
        }else{
          if(typeof(item.name)==='string' && item.name.indexOf(keyword)!==-1){
            return true
          }
          else if(typeof(item.code)==='string' && item.code.indexOf(keyword)!==-1){
            return true
          }
          else{
            if(typeof(item.name.toString())==='string' && item.price.toString().indexOf(keyword)!==-1){
              return true
            }else{
              return false
            }
          }
        }
      }).map((image) => (
        <ButtonBase
          onClick={()=>handleAdd(image)}
          focusRipple
          key={image.id}
          className={classes.image}
          focusVisibleClassName={classes.focusVisible}
          style={{
            width: '25%',
          }}
        >
          <span
            className={classes.imageSrc}
            style={{
              backgroundImage: `url(${image.img})`,
            }}
          />
          <span className={classes.imageBackdrop} />
          <span className={classes.imageButton}>
            <Typography
              component="span"
              variant="subtitle1"
              color="inherit"
              className={classes.imageTitle}
            >
              {image.name} - {image.price}
              <span className={classes.imageMarked} />
            </Typography>
          </span>
        </ButtonBase>
      ))}
    </div>
    </>
  );
}
