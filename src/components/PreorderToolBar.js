import React, { useState, useEffect } from 'react';
import {
    Typography,
    FormControlLabel,
    Grid,
    FormControl,
    Checkbox,
    FormHelperText,
    TextField,
    Snackbar,
    Button,
    TableRow,
    TableCell,
    Table,
    TableContainer,
    Paper,
    TableHead,
    TableBody,
} from '@material-ui/core'
import {
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import MuiAlert from '@material-ui/lab/Alert';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux'
import TablePreoderList from '../components/TablePreoderList'
import TablePreorderSummary from '../components/TablePreorderSummary'
import {
    useFirestoreCollectionData,
    useFirestore
  } from 'reactfire'

import PickDate from '../components/PickDate'
const PreorderToolBar = ()=>{
    const dispatch = useDispatch()
    const d = useSelector(state=>state.preorder.shippingDate)
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                <PickDate label="เลือกวันที่"
                 selectedDate={d} setSelectedDate={(v)=>{
                    dispatch({type:'PREORDER_SET_SHIPPINGDATE',value:v})
                }} />
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
        
    )
}
export default PreorderToolBar