import React, { useState, useEffect } from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  IconButton,
} from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import DialogProduct from './DialogProduct';
import {useFirestoreCollectionData, useFirestore,useStorage} from 'reactfire'
import { useDispatch,useSelector } from 'react-redux';

const TableProducts = ()=>{
  const firestore = useFirestore()
  const keyword = useSelector(state=>state.product.keyword)
  const dispatch = useDispatch()
  const storage = useStorage()
  const baseRef = firestore.collection('products')
  const rows = useFirestoreCollectionData(baseRef, { idField: 'id' })
  const [rowsWithImg,setRowsWithImg]=useState([])
  const [itemCount,setItemCount]=useState(0)
  const handleDelete = (row)=>{
    if(window.confirm(`ลบ ${row.name}?`)===true){
        baseRef.doc(row.id).delete()
    }
  }
  const handleEdit = (id)=>{
    dispatch({
        type:'PRODUCTS_SET_EDITDIALOG',
        value:true,
        id:id
    })
  }

  useEffect(()=>{
    const r = rows.map(async i=>{
      try{
        const url = await storage.ref('images').child(i.id).getDownloadURL()
        return {...i,img:url}
      }catch(e){
        return {...i,img:null}
      }
    })
    Promise.all(r)
    .then(result=>{
      setRowsWithImg(result)
    })
  },[rows])
  useEffect(()=>{
    const c = rows.reduce((s,item)=>{
      if(keyword===''){
        return s+1
      }else{
        if(typeof(item.name)==='string' && item.name.indexOf(keyword)!==-1){
          return s+1
        }
        else if(typeof(item.code)==='string' && item.code.indexOf(keyword)!==-1){
          return s+1
        }
        else{
          if(typeof(item.name.toString())==='string' && item.price.toString().indexOf(keyword)!==-1){
            return s+1
          }else{
            return s
          }
        }
      }
    },0)
    setItemCount(c)
  },[rows,keyword])
  return (
      <>
      <Typography variant="subtitle1" gutterBottom>
        Result: {itemCount} item(s)
      </Typography>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="saleList">
          <TableHead>
            <TableRow>
              <TableCell>code</TableCell>
              <TableCell>สินค้า</TableCell>
              <TableCell>รูป</TableCell>
              <TableCell align="right">ราคา</TableCell>
              <TableCell align="center">แก้ไข</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rowsWithImg.filter((item)=>{
              if(keyword===''){
                return true
              }else{
                if(typeof(item.name)==='string' && item.name.indexOf(keyword)!==-1){
                  return true
                }
                else if(typeof(item.code)==='string' && item.code.indexOf(keyword)!==-1){
                  return true
                }
                else{
                  if(typeof(item.name.toString())==='string' && item.price.toString().indexOf(keyword)!==-1){
                    return true
                  }else{
                    return false
                  }
                }
              }
            }).map((row) => (
              <>
              <TableRow key={row.id} >
              <TableCell component="th" scope="row">
                    {row.code}
                </TableCell>
                <TableCell >
                    {row.name}
                </TableCell>
                <TableCell>
                  <img src={row.img} style={{ height: '100px' }}/>
                </TableCell>
                <TableCell align="right">{row.price}</TableCell>
                <TableCell align="center">
                    <IconButton onClick={()=>{handleEdit(row.id)}}>
                        <EditIcon/>
                    </IconButton>
                    <IconButton onClick={()=>{handleDelete(row)}}>
                        <DeleteIcon/>
                    </IconButton>
                </TableCell>
              </TableRow>
            </>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <DialogProduct />
      </>
    );
}
export default TableProducts
