import React from 'react';
import {
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Collapse,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {useFirestoreCollectionData, useFirestore} from 'reactfire'
import moment from 'moment'

const SaleTable = ()=>{
  const firestore = useFirestore()
  const baseRef = firestore.collection('sale').orderBy("timestamp", "desc")
  const rows = useFirestoreCollectionData(baseRef, { idField: 'saleid' })

  return (
      <>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="saleList">
          <TableHead>
            <TableRow>
              <TableCell>วันเวลา</TableCell>
              <TableCell>ผู้ขาย</TableCell>
              <TableCell>ขายผ่าน</TableCell>
              <TableCell>รายการ</TableCell>
              <TableCell align="right">จำนวน</TableCell>
              <TableCell align="right">ราคา</TableCell>
              <TableCell align="right">รวม</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <>
              <TableRow key={row.id} >
                <TableCell component="th" scope="row">
                    {moment(row.timestamp.seconds*1000).calendar()}
                </TableCell>
                <TableCell>{row.seller}</TableCell>
                <TableCell>{row.saleType==='store'?'หน้าร้าน':row.saleType}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell align="right">{row.amount}</TableCell>
                <TableCell align="right">{row.price}</TableCell>
                <TableCell align="right">{row.price*row.amount}</TableCell>
                <TableCell align="right">

                </TableCell>
              </TableRow>
            </>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </>
    );
}
export default SaleTable
