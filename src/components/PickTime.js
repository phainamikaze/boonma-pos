import React from 'react';
import {
  KeyboardTimePicker,
} from '@material-ui/pickers';
import "moment/locale/th";
export default function MaterialUIPickers({selectedDate,setSelectedDate,label}) {
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
        <KeyboardTimePicker
        //   margin="normal"
          inputVariant="outlined"
          format="HH:mm"
          id="time-picker"
          label={label}
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}/>
  );
}
