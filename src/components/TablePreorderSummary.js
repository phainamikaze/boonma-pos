import React,{useEffect,useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {
    useFirestoreCollectionData,
    useFirestore
  } from 'reactfire';
const useStyles = makeStyles((theme) => ({
  root: {
   paddingBottom:theme.spacing(2)
  }
}));

const TablePreorderSummary = ({items}) => {
  const classes = useStyles();
  const firestore = useFirestore();
  const querypreorderProducts = firestore.collection('products').where('preorder','==',true )
  const preorderList = useFirestoreCollectionData(querypreorderProducts, { idField: 'id' });
  const [productsListWithCount , setProductsListWithCount] = useState({})
  useEffect(()=>{
    const r1 = items.reduce((s,i)=>{
      let s1 = {...s}
      s1.sumprice+=i.net
      s1.shippingCost+=i.shippingCost
      i.items.forEach((j)=>{
        if('amount' in j && j.amount>0){
          s1.count+=Number(j.amount)
          if(j.id in s1){
            s1[j.id] += Number(j.amount)
          }else{
            s1[j.id] = Number(j.amount)
          }
        }
      })
      return s1
    },{count:0,sumprice:0,shippingCost:0})
    setProductsListWithCount(r1)
  },[items])
  return (
    <Grid container className={classes.root}
      direction="row"
      justify="center"
      alignItems="center">
    <Grid item xs={12} sm={4}>
    <TableContainer component={Paper}>
      <Table size="small">
        <TableBody>
          <TableRow>
              <TableCell colSpan={2} align="center">สรุปรายการ</TableCell>
            </TableRow>
          {preorderList.map((item) => (
            <TableRow>
              <TableCell>{item.name}</TableCell>
              <TableCell align="right">{productsListWithCount[item.id]?productsListWithCount[item.id]:0}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </Grid>
    <Grid item xs={12} sm={4}>
      <Typography variant="subtitle1" gutterBottom align="center">
        รวม {productsListWithCount.count}
      </Typography>
    </Grid>
    <Grid item xs={12} sm={4}>
      <Typography variant="subtitle1" gutterBottom align="right">
        ยอดไม่รวมค่าส่ง {productsListWithCount.sumprice-productsListWithCount.shippingCost} บาท
      </Typography>
      <Typography variant="subtitle1" gutterBottom align="right">
        ค่าส่ง {productsListWithCount.shippingCost} บาท
      </Typography>
      <Typography variant="h3" gutterBottom align="right">
        ยอดรวม {productsListWithCount.sumprice} บาท
      </Typography>
    </Grid>
    </Grid>
  );
}
export default TablePreorderSummary