import React from 'react';
import {
  KeyboardDatePicker,
} from '@material-ui/pickers';
import "moment/locale/th";
export default function MaterialUIPickers({selectedDate,setSelectedDate,label}) {
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return (
    

        <KeyboardDatePicker
          //margin="normal"
          inputVariant="outlined"
          id="date-picker-dialog"
          label={label}
          format="dddd DD MMMM YYYY"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
  );
}
