import React,{ useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { useSelector, useDispatch } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import {
  useFirestoreCollectionData,
  useFirestore
} from 'reactfire';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide() {
    const dispatch = useDispatch()
    const firestore = useFirestore()
    const baseRef = firestore.collection('pendingSale')
    const baseRef2 = firestore.collection('sale')
    const open = useSelector(state=>state.sale.showDialogPay)
    const sum = useSelector(state=>state.sale.sum)
    const saleType = useSelector(state=>state.sale.saleType)
    const seller = useSelector(state=>state.sale.seller)
    const [cpay,setCpay] = useState(0)
    const handleClose = () => {
        dispatch({
            type:'SALE_SHOW_DIALOGPAY',
            value:false
        })
    };
    const pendingSale = useFirestoreCollectionData(baseRef, { idField: 'id' })
    const handleFocus = (event) => event.target.select();
    useEffect(()=>{
      setCpay(sum)
    },[sum,open])
    const handlePay = ()=>{
      const txid = uuidv4();
      const saveItem = pendingSale.map(item=>{
        return baseRef2.add({
          amount:item.amount,
          name:item.name,
          price:item.price,
          productId:item.id,
          saleType:saleType,
          txid:txid,
          seller:seller,
          timestamp: new Date(),
        })
      })
      Promise.all(saveItem).then(()=>{
        pendingSale.map(item=>{
          baseRef.doc(item.id).delete()
        })
      }).finally(()=>{
        handleClose()
      })
      
    }
  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">ยืนยันการชำระเงิน</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
          <Grid container
            direction="row"
            justify="space-between"
            alignItems="center">
            <Grid item xs={6}>
              <Typography variant="h4" component="h2" gutterBottom>
                รวม : 
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h4" component="h2" gutterBottom>
                {sum}
              </Typography>
            </Grid>
          </Grid>
          <Typography variant="h6" component="h2" gutterBottom>
            ขายผ่าน : {saleType==='store'?'หน้าร้าน':saleType}
          </Typography>
          <TextField id="cpay" name="cpay" 
          label="รับเงิน" autoFocus={true}
          variant="outlined" defaultValue={cpay}
          type="number" value={cpay} onFocus={handleFocus}
          fullWidth onChange={(e)=>{setCpay(e.target.value)}} />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button size="large" onClick={handleClose} color="primary">
            ยกเลิก
          </Button>
          <Button size="large" variant="contained" color="primary" fullWidth onClick={handlePay}>
            ชำระเงิน
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
