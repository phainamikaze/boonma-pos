import React, { useState, useEffect } from 'react';
import {
    Typography,
    FormControlLabel,
    Grid,
    Checkbox,
    TextField,
    Snackbar,
    Button,
    TableRow,
    TableCell,
    Table,
    TableContainer,
    Paper,
    TableHead,
    TableBody,
} from '@material-ui/core'
import moment from 'moment'
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import MuiAlert from '@material-ui/lab/Alert'
import {
    useFirestoreCollectionData,
    useFirestore,
  } from 'reactfire'
import { useHistory,useParams } from "react-router-dom"
import PickDate from './PickDate'
import PickTime from './PickTime'
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const PreorderForm = ()=>{
    const firestore = useFirestore()
    const history = useHistory();
    let { preOrderId } = useParams();
    const baseRef = firestore.collection('preorder')
    const querypreorderProducts = firestore.collection('products').where('preorder','==',true )
    const preorderProducts = useFirestoreCollectionData(querypreorderProducts, { idField: 'id' });
    const [itemList,setItemList] = useState([])
    const [skber,setskber] = useState({
        ok:false,
        error:false,
    })
    const handleCloseskber = ()=>{
        setskber({
            ok:false,
            error:false,
        })
    }
    const initDataInput = {
        cname:'',
        cphone:'',
        cfb:'',
        clocation:'',
        cremark:'',
        shippingDate: new Date(moment().add(1, 'days')),
        shipping:false,
        shippingCost:0,
        discount:false,
        discountCost:0,
        sum:0,
        sumamount:0,
        CoconutSoftRoll:0,
    }

    const [dataInput,setDataInput] = useState(initDataInput)
    useEffect(()=>{
        if(preOrderId){
            baseRef.doc(preOrderId).get().then(result=>{
                console.log(result.data())
                const dataItem = result.data()
                const updateData = {
                    cname:dataItem.cname,
                    cphone:dataItem.cphone,
                    cfb:dataItem.cfb,
                    clocation:dataItem.clocation,
                    cremark:dataItem.cremark,
                    shippingDate: new Date(dataItem.shippingDate),
                    shipping:false,
                    shippingCost:0,
                    discount:false,
                    discountCost:0,
                    sum:0,
                    sumamount:0,
                    CoconutSoftRoll:0,
                }
                setDataInput(updateData)
            })
        }
        
    },[preOrderId])
    const [discountList,setDiscountList] = useState([])
    const [net,setNet] = useState(0)
    const handleFocus = (event) => event.target.select();
    const handleSelect = (e) =>{
        const items = itemList.map(i=>{
            if(i.id===e.target.name){
                const amount = i.amount>0?i.amount:1
                return {
                    ...i,
                    amount:e.target.checked?amount:0,
                    checked:e.target.checked,
                }
            }else{
                return {...i}
            }
        }).map(i=>{
            if(i.amount>=0){
                return {
                    ...i,
                    sumprice:i.amount*i.price
                }
            }else{
                return {...i}
            }
        })
        Promise.all(items).then((result)=>{
            setItemList(result)
        })
    }
    const CalPrice = () =>{
        let dcl = []
        let dc = 0
        if(dataInput.CoconutSoftRoll>=5){
            for(let i = 0;i<(Math.floor(dataInput.CoconutSoftRoll/5));i++){
                dcl.push({desc:'ส่วนลด ทองม้วน 5 กล่อง',value:-25})
                dc+=25
            }

        }
        setDiscountList(dcl)
        setNet(Number(dataInput.sum)+Number(dataInput.shippingCost)-Number(dataInput.discountCost)-dc)
    }
    const handleChange = e => {
        e.persist()
        setDataInput(old=>({
            ...old,
            [e.target.name]:e.target.value
        }))
    }
    const handleChangeProducts = e =>{
        const items = itemList.map(i=>{
            if(`${e.target.name}-${i.id}`===e.target.id){
                if(e.target.name==='sumprice'){
                    return {
                        ...i,
                        sumprice:Number(e.target.value)
                    }
                }
                else if(e.target.name==='amount'){
                    return {
                        ...i,
                        amount:e.target.value,
                        sumprice:Number(e.target.value)*i.price
                    }
                }
                else{
                    return {
                        ...i,
                    }
                }
                
            }else{
                return {
                    ...i,
                }
            }
        })
        setItemList(items)
    }
    const handleSave = ()=>{
        baseRef.add({
            ...dataInput,
            items: [...itemList],
            net:net,
            status:1,
            createAt: new Date()
        }).then(()=>{
            const t = preorderProducts.map(i=>({
                ...i,
                checked:false,
                sumprice:0,
                amount:0,
            }))
            Promise.all(t).then((tt)=>{
                setItemList(
                    [...t]
                )
                
            })
            setDataInput({...initDataInput})

            setskber(old=>({
                ...old,
                ok:true
            }))
            setTimeout(()=>{
                history.push("/preorderdelivery");
            },1000)
            
        })
    }
    const handleSelectShipping = (e) =>{
        e.persist();
        if(e.target.name === 'shippingCost'){
            setDataInput(old=>{
                return {
                    ...old,
                    shippingCost: Number(e.target.value),
                    shipping:true,
                }
            })
        }
        else if(e.target.name=== 'shipping'){
            setDataInput(old=>{
                if(old.shipping===true){
                    return {
                        ...old,
                        shipping: !old.shipping,
                        shippingCost:0,
                    }
                }else{
                    return {
                        ...old,
                        shipping: !old.shipping,
                        shippingCost:20
                    }
                }
                
            })
        }
        else if(e.target.name === 'discount'){
            setDataInput(old=>{
                if(old.discount===true){
                    return {
                        ...old,
                        discount: !old.discount,
                        discountCost:0
                    }
                }else{
                    return {
                        ...old,
                        discount: !old.discount,
                        discountCost:0
                    }
                }
            })
        }
        else if(e.target.name === 'discountCost'){
            setDataInput(old=>{
                if(Number(e.target.value)>0){
                    return {
                        ...old,
                        discount: true,
                        discountCost:Number(e.target.value)
                    }
                }
                
            })
        }
        setNet(dataInput.sum+Number(dataInput.shippingCost)-Number(dataInput.discountCost))
        
        
    }
    useEffect(()=>{
        setItemList(
            [...preorderProducts]
        )
    },[preorderProducts])
    useEffect(()=>{
        CalPrice()
    },[dataInput])
    useEffect(()=>{
        const sp = itemList.reduce((s,i)=>{
            if(i.checked===true){
                return {
                    sum:(s.sum+Number(i.sumprice)),
                    itemcount:(s.itemcount+Number(i.amount)),
                    CoconutSoftRoll:(i.id==='HZSBQEybSTveLHGV2BZq'||i.id==='poTnvkmFESacwD43n6Bs'||i.id==='IaFZO1zG4feUD11TwHpX')?s.CoconutSoftRoll+Number(i.amount):Number(s.CoconutSoftRoll)
                }
            }else{
                return {...s}
            }
        },{
            sum:0,
            itemcount:0,
            CoconutSoftRoll:0
        })
        setDataInput(old=>({
            ...old,
            sum:sp.sum,
            sumamount:sp.itemcount,
            CoconutSoftRoll:sp.CoconutSoftRoll,
        }))
        CalPrice()
    },[itemList])
    return (
        <>
        <Snackbar open={skber.ok} autoHideDuration={6000} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} onClose={handleCloseskber}>
            <Alert onClose={handleCloseskber} severity="success">
                This is a success message!
            </Alert>
        </Snackbar>
        <MuiPickersUtilsProvider utils={MomentUtils}>
        <Grid container spacing={1}>
            <Grid item xs={12} sm={4}>
                <TextField id="cname" name="cname" label="ชื่อลูกค้า" variant="outlined" value={dataInput.cname} fullWidth onChange={handleChange} autoFocus={true}/>
            </Grid>
            <Grid item xs={12} sm={4}>
                <TextField id="cphone" name="cphone" label="เบอร์โทร" variant="outlined" value={dataInput.cphone} fullWidth onChange={handleChange} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <TextField id="cfb" name="cfb" label="facebook" variant="outlined" value={dataInput.cfb} fullWidth onChange={handleChange}/>
            </Grid>
            <Grid item xs={12} sm={3}>
                <PickDate label={`กำหนดส่ง`} selectedDate={dataInput.shippingDate} setSelectedDate={(v)=>{
                    console.log(v.format())
                    setDataInput(old=>({
                        ...old,
                        shippingDate:v._d
                    }))
                }} />
            </Grid>
            <Grid item xs={12} sm={3}>
                <PickTime label={`เวลาส่ง`} selectedDate={dataInput.shippingDate} setSelectedDate={(v)=>{
                    setDataInput(old=>({
                        ...old,
                        shippingDate:v._d
                    }))
                }} />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField id="clocation" name="clocation" label="พิกัด" variant="outlined" value={dataInput.clocation} fullWidth onChange={handleChange} />
            </Grid>
            <Grid item xs={12}>
                <TextField id="cremark" name="cremark" label="รายละเอียด" variant="outlined" value={dataInput.cremark} fullWidth onChange={handleChange} />
            </Grid>
            <Grid item xs={12}>
                <TableContainer component={Paper}>
                <Table size="small" aria-label="a dense table">
                    <TableHead>
                    <TableRow>
                        <TableCell>สินค้า</TableCell>
                        <TableCell>จำนวน</TableCell>
                        <TableCell>ราคา</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {itemList.map((row) => (
                        <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            <FormControlLabel
                                control={<Checkbox size="medium" checked={row.checked} onChange={handleSelect} name={row.id} />}
                                label={row.name}
                            />
                        </TableCell>
                        <TableCell>
                            <TextField id={`amount-${row.id}`} name="amount" onFocus={handleFocus}
                            label={row.unit} defaultValue={0} value={row.amount} type="number" variant="outlined" onChange={handleChangeProducts} />
                        </TableCell>
                        <TableCell>
                            <TextField id={`sumprice-${row.id}`} name="sumprice" 
                            inputProps={{readOnly:true}}
                            label="บาท" defaultValue={0} value={row.sumprice} type="number" variant="outlined" onChange={handleChangeProducts} />
                        </TableCell>
                        </TableRow>
                    ))}
                        <TableRow key="sum">
                            
                            <TableCell align="right" component="th" scope="row">
                                <Typography variant="h6" display="block" gutterBottom>รวม</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography variant="subtitle1" display="block" gutterBottom>{dataInput.sumamount}</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography variant="subtitle1" display="block" gutterBottom>{dataInput.sum}</Typography>
                            </TableCell>
                        </TableRow>
                        {discountList.map((dci,i)=>{
                            return (
                                <TableRow key={`dci-${i}`}>
                                    <TableCell align="right" component="th" scope="row">
                                    </TableCell>
                                    <TableCell>
                                        <Typography variant="subtitle1" display="block" gutterBottom>{dci.desc}</Typography>
                                    </TableCell>
                                    <TableCell>
                                        <Typography variant="subtitle1" display="block" gutterBottom>{dci.value}</Typography>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                        <TableRow key="shipping">
                            <TableCell></TableCell>
                            <TableCell align="right" component="th" scope="row">
                                <FormControlLabel
                                    labelPlacement="start"
                                    control={<Checkbox size="medium" checked={dataInput.shipping} 
                                    value={dataInput.shipping} onChange={handleSelectShipping} name="shipping" />}
                                    label="ค่าส่ง"
                                />
                            </TableCell>
                            <TableCell>
                                <TextField id="shippingCost" name="shippingCost" label="บาท" defaultValue={0} onFocus={handleFocus}
                                value={dataInput.shippingCost} type="number" variant="outlined" onChange={handleSelectShipping} />
                            </TableCell>
                        </TableRow>
                        <TableRow key="discount">
                            <TableCell></TableCell>
                            <TableCell align="right" component="th" scope="row">
                                <FormControlLabel
                                    labelPlacement="start"
                                    control={<Checkbox size="medium" checked={dataInput.discount} value={dataInput.discountValue} onChange={handleSelectShipping} name="discount" />}
                                    label="ส่วนลดพิเศษ"
                                />
                            </TableCell>
                            <TableCell>
                                <TextField id="discountCost" name="discountCost" label="บาท" defaultValue={0} onFocus={handleFocus}
                                value={dataInput.discountCost} type="number" variant="outlined" onChange={handleSelectShipping} />
                            </TableCell>
                        </TableRow>
                        <TableRow key="net">
                            <TableCell></TableCell>
                            <TableCell align="right" component="th" scope="row">
                                <Typography variant="h6" display="block" gutterBottom>รวมทั้งสิ้น</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography variant="h2" display="block" gutterBottom>{net}</Typography>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                </TableContainer>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" color="primary" size="large" onClick={handleSave} fullWidth>
                    บันทึก
                </Button>
            </Grid>
            
        </Grid>
        </MuiPickersUtilsProvider>
        </>
    )
}

export default PreorderForm